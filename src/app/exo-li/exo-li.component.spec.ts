import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExoLiComponent } from './exo-li.component';

describe('ExoLiComponent', () => {
  let component: ExoLiComponent;
  let fixture: ComponentFixture<ExoLiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExoLiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExoLiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
