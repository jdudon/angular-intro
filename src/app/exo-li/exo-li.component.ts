import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'simplon-exo-li',
  templateUrl: './exo-li.component.html',
  styleUrls: ['./exo-li.component.css']
})
export class ExoLiComponent implements OnInit {

  list = ["ga", "zo"];
  todo = ""
  constructor() { }

  addItem() {
    this.list.push(this.todo);
  }

  rmItem(index: number) {
    this.list.splice(index, 1);
  }

  changeOrder(index: number, direction: string) {
    if (direction == "down") {
      this.list.splice(index + 2, 0, this.list[index]);
      this.list.splice(index, 1);
    }

    else {
      this.list.splice(index - 1, 0, this.list[index]);
      this.list.splice(index + 1, 1);
    }
  }

  ngOnInit() {
  }

}
