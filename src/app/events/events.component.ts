import { Component, OnInit } from '@angular/core';
import { EventsService } from '../services/events/events.service';
import { Events } from '../entity/events';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css']
})
export class EventsComponent implements OnInit {
  events:Events[] = [];

  constructor(private service:EventsService) { }

  ngOnInit() {
    this.service.findAll().subscribe(data=> this.events = data);
  }

}
