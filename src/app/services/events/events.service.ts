import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Events } from '../../entity/events';
import { map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class EventsService {

  private url= 'http://localhost:3000/event';

  constructor(private http:HttpClient) {
   }

  findAll(): Observable<Events[]> {
    return this.http.get<Events[]>(this.url)
        .pipe(
          map(data => {
            for(let event of data) {
              event.date = new Date(event.date*1000);
            }
            return data;
          })
        )
  }
}
