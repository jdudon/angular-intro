import { Component } from "@angular/core";


@Component({
  templateUrl: './first.component.html',
  selector: 'simplon-first'
})
export class FirstComponent {
  maVariable = 'Pouit';
  hidePara = false;
  imgSrc = 'https://www.wanimo.com/veterinaire/images/articles/chat/chaton-qui-miaule.jpg';
  togglePara() {
    this.hidePara = !this.hidePara;
  }
  ;
}