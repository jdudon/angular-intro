import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from "@angular/forms";
import { NgModule } from '@angular/core';
import { HttpClientModule } from "@angular/common/http";

import { AppComponent } from './app.component';
import { FirstComponent } from './first/first.component';
import { ExoTemplateComponent } from './exo-template/exo-template.component';
import { StructureComponent } from './structure/structure.component';
import { ExoLiComponent } from './exo-li/exo-li.component';
import { Routes, RouterModule } from "@angular/router";
import { NotFoundComponent } from './not-found/not-found.component';
import { ObservableComponent } from './observable/observable.component';
import { EventsComponent } from './events/events.component';

const routes:Routes = [
  {path: 'first', component: FirstComponent },
  {path: 'exo-li', component: ExoLiComponent },
  {path: 'exo-template', component: ExoTemplateComponent },
  {path: 'structure', component: StructureComponent },
  {path: 'observable', component: ObservableComponent},
  {path: 'observable', component: ObservableComponent},
  {path: 'events', component:EventsComponent},
  {path: '', redirectTo: '/first', pathMatch: 'full'},
  {path: '**', component: NotFoundComponent}
];


/**
 * Le NgModule représente un Module Angular, c'est à dire un groupe
 * de Components et de Services (équivalent d'un Bundle Symfony).
 * Tous les Components et Services du module doivent être déclaré
 * dans la partie declarations de celui ci.
 * Si le module utilise d'autres modules, il faudra déclarer ceux ci
 * dans la partie imports.
 * La partie bootstrap (rien à voir avec la librairie css) représente
 * le (ou les) component racine qui seront chargés automatiquement
 * au lancement de l'application.
 */
@NgModule({
  declarations: [
    AppComponent,
    FirstComponent,
    ExoTemplateComponent,
    StructureComponent,
    ExoLiComponent,
    NotFoundComponent,
    ObservableComponent,
    EventsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(routes),
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
